import boto3

TABLENAME_CST = 'iot_senso'
# Get the service resource.
dynamodb = boto3.resource('dynamodb')

# Create the DynamoDB table.
table = dynamodb.create_table(
    TableName=TABLENAME_CST,
    KeySchema=[
        {
            'AttributeName': 'senso_id',
            'KeyType': 'HASH'
        },
        {
            'AttributeName': 'date',
            'KeyType': 'RANGE'
        }
    ],
    AttributeDefinitions=[
        {
            'AttributeName': 'senso_id',
            'AttributeType': 'S'
        },
        {
            'AttributeName': 'date',
            'AttributeType': 'S'
        }
    ],
    ProvisionedThroughput={
        'ReadCapacityUnits': 1,
        'WriteCapacityUnits': 1
    }
)

# Wait until the table exists.
table.meta.client.get_waiter('table_exists').wait(TableName=TABLENAME_CST)

# Print out some data about the table.
print(table.item_count)
