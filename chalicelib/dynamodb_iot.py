import boto3

dynamodb =  boto3.resource("dynamodb")
table = dynamodb.Table('iot_senso')


def push_data(data):
    if 'payload_cleartext' in data:
        item = {
            "date": data['timestamp'],
            "senso_id": str(data['device']),
            "payload_cleartext": data['payload_cleartext']
        }
        return table.put_item(Item=item)