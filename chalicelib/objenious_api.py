import requests

message_url = "https://api.objenious.com/v1/devices/%s/messages"
apikey = "riDRU5ggUqoUID/TwqFmBCgFouoQELs6n4Y6lqTHPdQ="


def get_data(app, id):
    url = message_url % id
    try:
        response = requests.get(url, headers={"apikey": apikey})
        return response
    except Exception, e:
        error = "Error while retrieving data for id: %s" % id
        app.log.error(error)