import chalice
import requests
import json
import traceback
import logging
from chalicelib import objenious_api, dynamodb_iot


app = chalice.Chalice(app_name='objenious')
ids = ["72902018968059908", "72902018968059909","72902018968059910", "72902018968059911", "72902018968059912",
           "72902018968059913", "72902018968059914", "72902018968059915", "72902018968059916", "72902018968059917"]
app.log.setLevel(logging.DEBUG)


def catch_exception(app):
    def exception_decorator(func):
        def new_func(*args, **kw):
            try:
                return func(*args, **kw)
            except Exception as e:
                exception = traceback.format_exc()
                app.log.error(e)
                app.log.error(exception)
                return chalice.Response(body='an exception occured: %s\n%s' % (e, exception),
                                status_code=500,
                                headers={'Content-Type': 'text/plain'})
        return new_func
    return exception_decorator


@app.route('/ids')
@catch_exception(app)
def senso_ids():
    return chalice.Response(body=json.dumps({"senso_ids": ids}),
                            status_code=200,
                            headers={'Content-Type': 'application/json'})


@app.route('/loads')
@catch_exception(app)
def senso_ids():
    map(pull_data, ids)
    return chalice.Response(body=json.dumps({"data": "loaded"}),
                            status_code=200,
                            headers={'Content-Type': 'application/json'})


@app.schedule('rate(1 day)')
def retrieve_data(event):
    map(pull_data, ids)


def pull_data(id):
    data = objenious_api.get_data(app, id)
    for message in data.json()["messages"]:
        saved_input = dynamodb_iot.push_data(message)
    return


